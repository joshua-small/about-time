
import { PseudoClock } from "../PseudoClock";
import { DTMod } from "./DTMod";
import { ElapsedTime } from "../ElapsedTime";

let warn = (...args) => {
  if (ElapsedTime.debug) console.warn("about-time | ", ...args)
}

let log = (...args) => {
  console.log("about-time | ", ...args);
}

var compatShim = true;

export function clockStatus() {
  //@ts-ignore
  return window.SimpleCalendar.api.clockStatus()
}
export function secondsToInterval(seconds) {
  //@ts-ignore
  const interval = window.SimpleCalendar.api.secondsToInterval(secondds);
  // compat shim
  return intervalSCtoAT(interval);
}

export function currentWorldTime() {
  //@ts-ignore
  return game.time.worldTime;
  // look at window.SimpleCalendar.api.timestamp()
}

export function timestamp() {
  //@ts-ignore
  return window.SimpleCalendar.api.timestamp()
}

export function dateToTimestamp(date: {}) {
  date = intervalATtoSC(date);
  //@ts-ignore
  return window.SimpleCalendar.api.dateToTimestamp(date);
}
export function intervalATtoSC(interval) {
  const newInterval: {year?, month?, day?, hour?, minute?, second?} = {};
  // if (compatShim && ((interval.years || interval.months || interval.days || interval.hours || interval.minutes || interval.seconds) !== undefined)) {
  if (interval.years ?? interval.months ?? interval.days ?? interval.hours ?? interval.minutes ?? interval.seconds) {
    warn("About time | DT Mod notation has changed plese use .year/.month/.day/.hour/.minute/.sceond", interval)
    warn("About time | DT Mod deprecated - use SimpleCalendar.api instead");
  }

  newInterval.year = interval.year ?? interval.years;
  newInterval.month = interval.month ?? interval.months;
  newInterval.day = interval.day ?? interval.days;
  newInterval.hour = interval.hour ?? interval.hours;
  newInterval.minute = interval.minute ?? interval.minutes;
  newInterval.second = interval.second ?? interval.seconds;

  // }
  return newInterval;
}

export function intervalSCtoAT(interval) {
  const newInterval: {years?, months?, days?, hours?, minutes?, seconds?} = {};
  if (compatShim) {
    newInterval.years = interval.year ?? interval.years;
    newInterval.months = interval.month ?? interval.months;
    newInterval.days = interval.day ?? interval.days;
    newInterval.hours = interval.hour ?? interval.hours;
    newInterval.minutes = interval.minute ?? interval.minutes;
    newInterval.seconds = interval.second ?? interval.seconds;
  }
  return newInterval;

}

export function padNumber(n: number, digits = 2): string {
  return `${n}`.padStart(digits, "0");
}